-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `short_description` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `article` (`id`, `caption`, `image`, `content`, `short_description`, `date`) VALUES
(1,	'У меня есть мечта!',	'/upload/image/article/6LHjpX.jpg',	'<p><em>На протяжении всей своей жизни я хотел только одного.</em><br />\r\nЧто именно?<br />\r\n<strong>Создать продукт который останется после моей смерти.</strong><br />\r\nЯ задавал себе вопрос.<br />\r\nКто я?<br />\r\n<em>Я, Денис Альфа &mdash; ведущий агент по Продаже/Аренде/Инвестициям в недвижимости.</em><br />\r\nЧто я умею хорошо делать?<br />\r\n<em>Создавать продукты в недвижимости, продавать их, сдавать, привлекать инвестиции.</em></p>\r\n\r\n<p>Я долго не находил ответы.<br />\r\nВсе продукты к которым я имел отношения были хорошими, но локальными.</p>\r\n\r\n<p><a href=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/03/j.jpg?ssl=1\"><img alt=\"j\" src=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/03/j.jpg?w=960&amp;ssl=1\" style=\"height:1172px; width:671px\" /></a></p>\r\n\r\n<p>А я хотел чего-то глобального.<br />\r\n<strong>Продукт, который будет взаимодействовать с разными цивилизациями, религиями, философиями, континентами.</strong></p>\r\n\r\n<p>И вот произошло чудо!<br />\r\nГод 2016 стал для меня переломным.<br />\r\nОн приблизил меня к моей мечте.<br />\r\nМеня познакомили с самыми яркими и известными в мире людьми в нише недвижимости.<br />\r\nПосле ряда встреч и консультаций меня пригласили в удивительный проект под названием Guest House.<br />\r\nСейчас моя задача вести риелторский отдел и трафик отдел.<br />\r\nЯ обожаю недвижимость и маркетинг.<br />\r\nИ в этом продукте полностью осуществилась моя мечта.</p>\r\n\r\n<p><strong>Guest House &mdash;</strong>&nbsp;это квартиры в парковой территории.</p>\r\n\r\n<p><strong>Guest House &mdash;</strong>&nbsp;это продукт уникальный, как внутри, так и снаружи.<br />\r\nСвоя философия пространства.<br />\r\nСвой эксклюзивный микроклимат для проживания.</p>\r\n\r\n<p><strong>Guest House: живи|сдавай|инвестируй!</strong></p>\r\n\r\n<p><strong>Guest House: счастье|радость|любовь!</strong></p>\r\n\r\n<p><a href=\"https://i2.wp.com/denysalfa.com/wp-content/uploads/2017/03/sh.jpg?ssl=1\"><img alt=\"sh\" src=\"https://i2.wp.com/denysalfa.com/wp-content/uploads/2017/03/sh.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:377px; width:671px\" /></a></p>\r\n\r\n<p><em>Нет ничего лишнего.</em><br />\r\n<em>Комфорт просматривается в каждом элементе квартиры, дома, территории.</em></p>\r\n\r\n<p><strong>Guest House</strong>&nbsp;создали простым, понятным и доступным для каждого.<br />\r\nОсновная задача: покорить весь мир!<br />\r\nСтать именем нарицательным:<br />\r\n&laquo;Живи в Guest House&raquo;.<br />\r\nТаким же нарицательным как, &laquo;едь на Uber&raquo;, &laquo;путешествуй с Airbnb&raquo;.</p>\r\n\r\n<p><strong>Guest House</strong>&nbsp;настолько понятный продукт, что уже сейчас квартиру в нём можно купить через интернет, не выходя из дому.<br />\r\nВыбирай&nbsp;<strong>Guest House</strong>&nbsp;в городе, где ты живёшь, или где тебе нравится жить или иметь недвижимость.<br />\r\nРазмер квартиры:<br />\r\n3 м на 7 м, т.е. S: 21 кв.м (но всегда площадь квартиры можно кратно увеличить, т.е. сделать 42 или 63 или 84 или 105 метров).<br />\r\nВысота в квартире 3 метра.<br />\r\nВ доме 3 этажа, 19 квартир на этаже.</p>\r\n\r\n<p><strong>Guest House</strong>&nbsp;строится по всему миру из одного материала.<br />\r\nБританские нано технологии позволяют строить качественно и быстро.<br />\r\nВысокие стандарты тепло и звукоизоляции.<br />\r\nВысокая сейсмостойкость.</p>\r\n\r\n<p><a href=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/03/5-1.jpg?ssl=1\"><img alt=\"5\" src=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/03/5-1.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:671px; width:671px\" /></a></p>\r\n\r\n<p><strong>Guest House</strong>&nbsp;&mdash; это iPhone в недвижимости.<br />\r\nВ Европе, Америке, Азии, Африки, Австралии, каждый понимает, что такое Guest House.</p>\r\n\r\n<p>Я благодарен Вселенной за этот подарок!</p>\r\n\r\n<p>С уважением, Ваш Денис Альфа.</p>\r\n',	'На протяжении всей своей жизни я хотел только одного.\r\nЧто именно?\r\nСоздать продукт который останется после моей смерти.',	'2017-08-29 11:10:35'),
(2,	'Отчёт по Guest House (прошло 7 дней):',	'/upload/image/article/GHq_6c.jpg',	'<p><strong>Подробнее, что происходит:</strong><br />\r\n<strong>1.&nbsp;</strong><strong><em>Продукт уникальный и внеконкурентный.</em></strong><br />\r\nЭто подтвердили в марте все участники украинского рынка недвижимости: строители/риелтора/покупатели/арендаторы/инвесторы.</p>\r\n\r\n<p><a href=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/02/3.jpg?ssl=1\"><img alt=\"3\" src=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/02/3.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:671px; width:671px\" /></a><br />\r\n<strong><em>2. Продукт разрабатывали весь 2016 год в Лондоне лучшие специалисты в своём деле.</em></strong><br />\r\nСейчас он размножается, как вирус.<br />\r\nЕго уже не остановить.<br />\r\nПаспорта и коды в отдел продаж Денису Альфа сбрасывают даже с тех городов, где Guest House на запланирован на апрель и май 2017 года.</p>\r\n\r\n<p><a href=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/02/dream-home.jpg?ssl=1\"><img alt=\"dream-home\" src=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/02/dream-home.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:616px; width:671px\" /></a><br />\r\n<strong><em>3. Бренд и продукт новый, поэтому в голове у каждого идёт борьба страха и жадности.</em></strong><br />\r\nСтрах говорит, что есть высокие риски.<br />\r\nЖадность говорит, что в твоём городе проект раскупят и если ты не успеешь, то сильно переплатишь спекулятивным инвесторам.</p>\r\n\r\n<p><a href=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/02/12343.jpg?ssl=1\"><img alt=\"Альфа Одесса\" src=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/02/12343.jpg?w=960&amp;ssl=1\" style=\"height:729px; width:671px\" /></a><br />\r\n<em><strong>4. Спустя всего 7 дней квартирами в Guest House Odessa заинтересовались средние и крупные спекулятивные инвесторы.</strong></em><br />\r\nЭто значит, что продажи будут намного быстрее, чем ожидал отдел продаж и Денис Альфа.</p>\r\n\r\n<p><a href=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/02/tsj.jpg?ssl=1\"><img alt=\"tsj\" src=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/02/tsj.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:671px; width:671px\" /></a></p>\r\n\r\n<p><em><strong>5. Фонд и Краудинвесторы.</strong></em><br />\r\nСегодня Guest House Odessa предлагает краудинвесторам (от 1000 $) и инвесторам гарантированные 12% годовых в долларах при покупке квартиры.<br />\r\nЯ не знаю ни одной страны, где Вам предложат гарантированные 12%!</p>\r\n\r\n<p><a href=\"https://i2.wp.com/denysalfa.com/wp-content/uploads/2017/02/gri.jpg?ssl=1\"><img alt=\"Альфа Одесса\" src=\"https://i2.wp.com/denysalfa.com/wp-content/uploads/2017/02/gri.jpg?w=960&amp;ssl=1\" style=\"height:724px; width:671px\" /></a><br />\r\n<strong><em>6. Модель Guest House: 1 проект в 1 городе, нравится всем инвесторам.</em></strong><br />\r\nПотому что после всех первичных продаж, спекулятивные инвесторы могут максимально заработать при перепродаже на вторичном рынке.</p>\r\n\r\n<p><a href=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/01/6.jpg?ssl=1\"><img alt=\"Недвижимость\" src=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/01/6.jpg?w=960&amp;ssl=1\" style=\"height:894px; width:671px\" /></a><br />\r\n<em><strong>7. С Guest House Odessa заработали хорошие деньги много посредников (ласточки и риелтора).</strong></em><br />\r\nВ этом проекте всё просто|честно|понятно.<br />\r\nВремя запуска проекта, лучшее время, чтобы заработать, и купить себе новый автомобиль.</p>\r\n',	'Продукт уникальный и внеконкурентный.\r\nЭто подтвердили в марте все участники украинского рынка недвижимости: строители/риелтора/покупатели/арендаторы/инвесторы.',	'2017-08-29 11:17:11'),
(3,	'Проект Guest House. Как создали уникальную нанотехнологию?',	'/upload/image/article/7N77aK.jpg',	'<p><strong><em>Создавая проект Guest House</em></strong></p>\r\n\r\n<p><strong><em>в 2016 году перед британскими научными сотрудниками были поставлены три основные задачи:</em></strong></p>\r\n\r\n<p><strong>Температура.</strong></p>\r\n\r\n<p>Найти под данный проект материал и создать конструктив здания, который можно применять в любой стране, где будет построен проект Guest House, от Бали (где жаркий и влажный климат) до северных городов России (где сильные морозы и высокая коррозия).</p>\r\n\r\n<p><a href=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/01/4-1.jpg?ssl=1\"><img alt=\"4\" src=\"https://i0.wp.com/denysalfa.com/wp-content/uploads/2017/01/4-1.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:503px; width:671px\" /></a></p>\r\n\r\n<p><strong>Сейсмоустойчивость.</strong></p>\r\n\r\n<p>В решение этой задачи необходимо было включить высокую сейсмоустойчивость здания, если придётся строить Guest House в Японии.</p>\r\n\r\n<p>Также необходимо было учитывать подвижность грунтов, в случае строительства Guest House на мягких почвах.</p>\r\n\r\n<p><a href=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/01/Foto-Maristelly.jpg?ssl=1\"><img alt=\"foto-maristelly\" src=\"https://i1.wp.com/denysalfa.com/wp-content/uploads/2017/01/Foto-Maristelly.jpg?fit=960%2C7999&amp;ssl=1\" style=\"height:174px; width:671px\" /></a></p>\r\n\r\n<p><strong>Звукоизоляция.</strong></p>\r\n\r\n<p>Необходимо было решить задачу, в случае строительства Guest House в тёплых странах в курортных городах, где музыка играет настолько сильно, что подпрыгивает посуда на полках,</p>\r\n\r\n<p>как погасить звук и добиться полной тишины в квартире.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Британская нано технология прошла опробования и лабораторные испытания.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Были учтены все задачи, исправлены погрешности.</p>\r\n\r\n<p>Сегодня британская нанотехнология, которую используют при строительстве жилых массивов Guest House, позволяет при полной подготовке строить один этаж в день, без мокрых процессов.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>В данное время на рынке строительных технологий в мире нет подобного материала.</p>\r\n\r\n<p>Британская нано технология является запатентованной, не подлежит продаже и её используют только при строительстве жилых массивов Guest House.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Британские нанотехнологии &mdash; это лучшее качество Вашей недвижимости!</strong></p>\r\n',	'Создавая проект Guest House\r\nв 2016 году перед британскими научными сотрудниками были поставлены три основные задачи:',	'2017-08-29 11:18:58');

DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `url` text NOT NULL,
  `show_in_menu` int(11) NOT NULL,
  `can_guest` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `menu` (`id`, `label`, `url`, `show_in_menu`, `can_guest`, `sort`) VALUES
(2,	'Главная',	'{\"0\":\"site/index\"}',	0,	0,	0),
(3,	'блог',	'{\"0\":\"blog/index\"}',	1,	1,	1),
(4,	'Денис Альфа',	'{\"0\":\"site/about\"}',	0,	0,	2);

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base',	1503925471),
('m130524_201442_init',	1503925474);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1,	'some',	'E3z_9Vu0xKgRyoHOJY8whqLIsgjlf00u',	'$2y$13$cQhTudNNWrQFNCJKZTPKyOGUUPq1RiKWf39NbGLsXlbGZc8SBmvV.',	NULL,	'some@some.com',	10,	1503925612,	1503925612);

-- 2017-08-29 19:08:06
