<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 20:47
 */

namespace common\components;

class TextHelper
{
    // wrap matched string into span.match
    public static function showMatch($needle, $subject)
    {
        return str_replace($needle, "<span class='match'>$needle</span>", $subject);
    }
}