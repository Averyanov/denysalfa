<?

namespace common\components;

use common\models\Order;
use common\models\OrderItem;
use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
//use common\models\Order;

class UserRelationBehavior extends Behavior
{


    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete'
        ];
    }

    // events

    public function beforeValidate()
    {

    }

    public function afterInsert()
    {

    }

    public function afterUpdate()
    {

    }

    public function beforeDelete()
    {

    }

    public function getOrders()
    {
        return $this->owner->hasMany(Order::className(), ['owner_id' => 'id']);
    }

    public function getCurrentOrder()
    {
        // add condition for some status field
        return $this->getOrders()->one();
    }

    public function getOrderItems()
    {
        return $this->owner->hasMany(OrderItem::className(), ['owner_id' => 'id'])
            ->via('orders');
    }

}
