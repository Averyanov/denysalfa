<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tag".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ArticleTag[] $articleTags
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticleTags()
    {
        return $this->hasMany(ArticleTag::className(), ['tag_id' => 'id']);
    }

    public function getDataProvider()
    {
        return new ActiveDataProvider([ 'query' => $this->find() ]);
    }

    public function getHref($route = null)
    {
        $get = Yii::$app->request->get('tags') ?: [];

        $id = $this->id;

        $tags = in_array($id, $get)
            ? (array_filter($get, function ($i) use ($id) { return $i != $id; }))
            : array_merge($get, [$id]);

        return [
            implode('/', $route),
            'tags' => $tags
        ];
    }

    public function getClass($class = null)
    {
        return !in_array($this->id, (Yii::$app->request->get('tags') ?: [])) ?: $class;
    }
}
