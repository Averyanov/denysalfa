<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $label
 * @property string $url
 * @property integer $show_in_menu
 * @property integer $can_guest
 * @property integer $sort
 * @property integer $is_link
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'url', 'show_in_menu', 'can_guest', 'sort', 'can_user'], 'required'],
            [['url'], 'string'],
            [['show_in_menu', 'can_guest', 'can_user', 'sort', 'is_link'], 'integer'],
            [['label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Label',
            'url' => 'Url',
            'show_in_menu' => 'Show In Menu',
            'can_guest' => 'Can Guest',
            'sort' => 'Sort',
            'is_link' => 'Ссылка',
        ];
    }

    public function getDataProvider()
    {
        $query = $this->find()->where([ 'show_in_menu' => true])->orderBy('sort');

        if(Yii::$app->user->isGuest)
            $query->andWhere(['can_guest' => true]);
        else
            $query->andWhere(['can_user' => true]);


        return new ActiveDataProvider([ 'query' => $query ]);
    }
}
