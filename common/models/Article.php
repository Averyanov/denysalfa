<?php

namespace common\models;

use Yii;
use common\components\ImageBehavior;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $caption
 * @property string $image
 * @property string $content
 * @property string $short_description
 * @property string $date
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    public $img_size = [
        'width' => 500,
        'height' => 500
    ];

    public function behaviors()
    {
        return [
            ImageBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['image'], 'file', 'extensions'=>'jpg, jpeg, gif, png', 'maxSize' => 1024 * 1024 * 10],
            [['caption', 'content', 'short_description'], 'required'],
            [['content', 'short_description'], 'string'],
            [['date'], 'safe'],
            [['caption'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'caption' => 'Caption',
            'image' => 'Image',
            'content' => 'Content',
            'short_description' => 'Short Description',
            'date' => 'Date',
        ];
    }

    public function getDataProvider()
    {
        $tags_id = Yii::$app->request->get('tags');
        $query = $this->find();

        if($tags_id)
        {
            $query
                ->join('join',
                    'article_tag',
                    'article.id = article_tag.article_id')
                ->join('join',
                    'tag',
                    'tag.id = article_tag.tag_id')
                ->andWhere(['tag.id' => $tags_id]);
        }

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 10]
        ]);
    }
}
