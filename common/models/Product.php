<?php

namespace common\models;

use Yii;
use common\components\ImageBehavior;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $price
 * @property string $name
 * @property string $image
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    public $img_size = [
        'width' => 500,
        'height' => 500
    ];

    public function behaviors()
    {
        return [
            ImageBehavior::className()
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'image',
                'required',
                'when' => function($model){return $model->isNewRecord; },
                'whenClient' => '
                    function(){
                        return document.
                            getElementsByClassName("file-input")[0]
                            .classList
                            .contains("file-input-new");
                    }'
            ],
            [['image'], 'file', 'extensions'=>'jpg, jpeg, gif, png', 'maxSize' => 1024 * 1024 * 10],
            [['price', 'name'], 'required'],
            [['price'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'name' => 'Name',
            'image' => 'Image',
        ];
    }
}
