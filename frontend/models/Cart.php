<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 05.09.17
 * Time: 17:56
 */

namespace frontend\models;

use Yii;
use yii\data\ActiveDataProvider;

class Cart
{
    public $user;
    public $order;

    public function __construct()
    {
        $this->user = Yii::$app->user->identity;
        $this->order = $this->user->getCurrentOrder();
    }

    public function getOrderItemDataProvider()
    {
        return new ActiveDataProvider(['query' => $this->user->getOrderItems()]);
    }
}