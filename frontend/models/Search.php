<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 18:37
 */
namespace frontend\models;

use yii\base\Model;
use common\models\Article;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;

class Search extends Model
{
    public $search_string;
    public $dataProviders;

    public function getSearchable()
    {
        // model, where can search, add here
        return [
            'blog' => new Article,
        ];
    }

    public function getIgnorableAttr()
    {
        // attribures, which must be ignored, add here
        return ['image', 'date', 'sort', 'url', 'id'];
    }

    public function search($ss) // $ss => 'search_string'
    {
        $this->search_string = $ss;

        foreach ($this->searchable as $name => $model)
        {
            // get list of avaliable attributes for search
            $list = $model->getAttributes(null, $this->ignorableAttr);

            // get ActiveQuery obj
            $query = $this->buildQuery($model, $list);

            // push ActiveDataProvider obj to ArrayDataProviders array
            $this->dataProviders[ $name ] = new ActiveDataProvider(['query' => $query]);
        }

        return $this;
    }

    public function buildQuery($model, $list, $operand = 'like')
    {
        $query = $model->find();

        foreach ($list as $attr => $value)
        {
            // complect query for every attr
            $query->orWhere([$operand, $attr, $this->search_string]);
        }

        return $query;
    }
}