<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 10:29
 */
namespace frontend\controllers;

use common\models\Page;
use yii\web\Controller;

class PageController extends Controller
{
    public function actionView($url)
    {
        $model = (new Page())->find()->where(['url' => $url])->one();

        return $this->render('view', ['model' => $model]);
    }
}
