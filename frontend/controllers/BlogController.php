<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 10:29
 */
namespace frontend\controllers;

use common\models\Article;
use common\models\Tag;
use yii\web\Controller;

class BlogController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index', [
            'articleDataProvider' => (new Article)->dataProvider,
            'tagDataProvider' => (new Tag)->dataProvider,
        ]);
    }

    public function actionView($id)
    {
        $blog_model = (new Article)->find()->where($id)->one();

        return $this->render('view', ['model' => $blog_model]);
    }
}
