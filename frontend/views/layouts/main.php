<?php
//
//* @var $this \yii\web\View */
//* @var $content string */
//
use yii\helpers\Html;
//use yii\bootstrap\Nav;
//use yii\bootstrap\NavBar;
//use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
//use common\widgets\Alert;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

use common\models\Menu;
//
//AppAsset::register($this);
//?>
<?php //$this->beginPage() ?>
<!--<!DOCTYPE html>-->
<!--<html lang="--><?//= Yii::$app->language ?><!--">-->
<!--<head>-->
<!--    <meta charset="--><?//= Yii::$app->charset ?><!--">-->
<!--    <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
<!--    <meta name="viewport" content="width=device-width, initial-scale=1">-->
<!--    --><?//= Html::csrfMetaTags() ?>
<!--    <title>--><?//= Html::encode($this->title) ?><!--</title>-->
<!--    --><?php //$this->head() ?>
<!--</head>-->
<!--<body>-->
<?php //$this->beginBody() ?>
<!---->
<!--<div class="wrap">-->
<!--    --><?php
////    NavBar::begin([
////        'brandLabel' => 'My Company',
////        'brandUrl' => Yii::$app->homeUrl,
////        'options' => [
////            'class' => 'navbar-inverse navbar-fixed-top',
////        ],
////    ]);
////    $menuItems = [
////        ['label' => 'Home', 'url' => ['/site/index']],
////        ['label' => 'About', 'url' => ['/site/about']],
////        ['label' => 'Contact', 'url' => ['/site/contact']],
////    ];
////    if (Yii::$app->user->isGuest) {
////        $menuItems[] = ['label' => 'Signup', 'url' => ['/site/signup']];
////        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
////    } else {
////        $menuItems[] = '<li>'
////            . Html::beginForm(['/site/logout'], 'post')
////            . Html::submitButton(
////                'Logout (' . Yii::$app->user->identity->username . ')',
////                ['class' => 'btn btn-link logout']
////            )
////            . Html::endForm()
////            . '</li>';
////    }
////    echo Nav::widget([
////        'options' => ['class' => 'navbar-nav navbar-right'],
////        'items' => $menuItems,
////    ]);
////    NavBar::end();
//    ?>
<!---->
<!--    <div class="container">-->
<!--        --><?//= Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//        ]) ?>
<!--        --><?//= Alert::widget() ?>
<!--        --><?//= $content ?>
<!--    </div>-->
<!--</div>-->
<!---->
<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <p class="pull-left">&copy; My Company --><?//= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
<!--    </div>-->
<!--</footer>-->
<!---->
<?php //$this->endBody() ?>
<!--</body>-->
<!--</html>-->
<?php //$this->endPage() ?>

<?// ------------------------------------------------?>


<? AppAsset::register($this) ?>
<? $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="wide smoothscroll wow-animation">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">

    <!--[if lt IE 10]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->

    <? $this->head() ?>

</head>
<body>
<? $this->beginBody() ?>
<!-- The Main Wrapper -->
<div class="page">

    <!--========================================================
                              HEADER
    =========================================================-->
    <header class="page-header page-header1">
        <!-- Swiper -->
        <?= $this->render('/site/swiper') ?>
        <!-- END Swiper -->
        <!-- RD Navbar -->
        <div class="rd-navbar-wrap" >
            <nav class="rd-navbar" data-lg-stick-up-offset="700px" data-rd-navbar-lg="rd-navbar-static" >
                <div class="rd-navbar-inner">
                    <!-- RD Navbar Panel -->
                    <div class="rd-navbar-panel">
                        <!-- RD Navbar Toggle -->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar"><span></span></button>
                        <!-- END RD Navbar Toggle -->
                        <!-- RD Navbar Brand -->
                        <div class="rd-navbar-brand">
                            <a href="<?= Url::toRoute(['site/index']) ?>" class="brand-name primary-color">
                                <span class="text-light">Денис Альфа</span>
                                <span class="text-ubold text-primary brand-slogan">Продажа недвижимости</span>
                            </a>
                        </div>
                        <!-- END RD Navbar Brand -->
                    </div>
                    <!-- END RD Navbar Panel -->
                    <div class="rd-navbar-nav-wrap">

<!--                        --><?//= $this->render(Yii::$app->user->isGuest ? '/site/sign/login' : '/site/sign/logout') ?>
                        <!-- RD Navbar Search -->
                        <?= $this->render('search_form') ?>
                        <!-- END RD Navbar Search -->
                        <!-- RD Navbar Nav -->
                        <?= $this->render('/layouts/menu/list', ['dataProvider' => (new Menu())->dataProvider]) ?>
                        <? /*<ul class="rd-navbar-nav">
                            <li class="active line-1">
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <a href="index-1.html">About</a>
                                <!-- RD Navbar Dropdown -->
                                <ul class="rd-navbar-dropdown">
                                    <li>
                                        <a href="#">Testimonials</a>
                                    </li>
                                    <li>
                                        <a href="#">Properties</a>
                                        <ul class="rd-navbar-dropdown">
                                            <li>
                                                <a href='#'>All</a>
                                            </li>
                                            <li>
                                                <a href='#'>USA</a>
                                            </li>
                                            <li>
                                                <a href='#'>Canada</a>
                                            </li>
                                            <li>
                                                <a href='#'>Mexico</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">News</a>
                                    </li>
                                </ul>
                                <!-- END RD Navbar Dropdown -->
                            </li>
                        </ul>
                        */?>
                        <!-- END RD Navbar Nav -->
                    </div>
                </div>
                <?/*
                <div class="cart">
                    <?= Html::a(null, Url::toRoute(['site/cart'])) ?>
                </div>
                */?>
            </nav>
        </div>
        <!-- END RD Navbar -->
    </header>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main class="page-content">
        <?= $content ?>
    </main>
    <!--========================================================
                              FOOTER
    ==========================================================-->
    <?= $this->render('/layouts/footer') ?>

</div>
<? $this->endBody() ?>
</body>
</html>
<? $this->endPage() ?>
