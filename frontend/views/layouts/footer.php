<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 01.09.17
 * Time: 13:07
 */

use yii\helpers\Url;
?>
<footer class="page-footer">
    <div class="container">
        <ul class="inline-list row text-center">
            <li class="col-md-2 col-xs-2">
                <a class="icon-xs fa-google-plus" href="#"></a>
            </li>
            <li class="col-md-2 col-xs-2">
                <a class="icon-xs fa-twitter" href="#"></a>
            </li>
            <li class="col-md-2 col-xs-2">
                <a class="icon-xs fa-facebook" href="#"></a>
            </li>
            <li class="col-md-2 col-xs-2">
                <a class="icon-xs fa-instagram" href="#"></a>
            </li>
            <li class="col-md-2 col-xs-2">
                <a class="icon-xs fa-linkedin" href="#"></a>
            </li>
            <li class="col-md-2 col-xs-2">
                <a class="icon-xs fa-pinterest-square" href="#"></a>
            </li>
        </ul>
    </div>
    <div class="container">
        <div class="row row-xs-center">
            <div class="text-center">
                <!-- RD Navbar Brand -->
                <div class="rd-navbar-brand">
                    <a href="<?= Url::toRoute(['site/index']) ?>" class="brand-name primary-color">
                        <span class="text-light">Денис Альфа</span>
                        <span class="text-ubold text-primary brand-slogan">ПРОДАЖА НЕДВИЖИМОСТИ</span>
                    </a>
                </div>
                <!-- END RD Navbar Brand -->
                <div class="copyright">
                    Copyright &#169; <span id="copyright-year"></span> |
                    <a href='index-6.html'>Privacy Policy</a>
                    More Real Estate Templates at <a rel="nofollow" href="http://www.templatemonster.com/category.php?category=454&type=1" target="_blank">TemplateMonster.com</a>
                </div>
            </div>
        </div>
    </div>
</footer>
