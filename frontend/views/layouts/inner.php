<?
//
//* @var $this \yii\web\View */
//* @var $content string */
//

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\helpers\Url;
use common\models\Menu;
?>

<? AppAsset::register($this) ?>
<? $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="wide smoothscroll wow-animation">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no"/>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/images/favicon.png" type="image/png">

    <!--[if lt IE 10]>
    <script src="js/html5shiv.min.js"></script>
    <![endif]-->

    <? $this->head() ?>

</head>
<body>
<? $this->beginBody() ?>
<!-- The Main Wrapper -->
<div class="page">

    <!--========================================================
                              HEADER
    =========================================================-->
    <header class="page-header page-header2">
        <!-- RD Navbar -->
        <div class="rd-navbar-wrap" >
            <nav class="rd-navbar" data-rd-navbar-lg="rd-navbar-static">
                <div class="rd-navbar-inner">
                    <!-- RD Navbar Panel -->
                    <div class="rd-navbar-panel">
                        <!-- RD Navbar Toggle -->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar"><span></span></button>
                        <!-- END RD Navbar Toggle -->
                        <!-- RD Navbar Brand -->
                        <div class="rd-navbar-brand">
                            <a href="<?= Url::toRoute(['site/index']) ?>" class="brand-name primary-color">
                                <span class="text-light">Денис Альфа</span>
                                <span class="text-ubold text-primary brand-slogan">Продажа недвижимости</span>
                            </a>
                        </div>
                        <!-- END RD Navbar Brand -->
                    </div>
                    <!-- END RD Navbar Panel -->
                    <div class="rd-navbar-nav-wrap">

<!--                        --><?//= $this->render(Yii::$app->user->isGuest ? '/site/sign/login' : '/site/sign/logout') ?>

                        <!-- RD Navbar Search -->
                        <?= $this->render('search_form') ?>
                        <!-- END RD Navbar Search -->

                        <!-- RD Navbar Nav -->
                        <?= $this->render('/layouts/menu/list', ['dataProvider' => (new Menu())->dataProvider]) ?>
                        <!-- END RD Navbar Nav -->

                    </div>
                </div>

                <?/*
                <div class="cart">
                    <?= Html::a(null, Url::toRoute(['site/cart'])) ?>
                </div>
                */?>

            </nav>
        </div>
        <!-- END RD Navbar -->
    </header>
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main class="page-content">
        <?= $content ?>
    </main>
    <!--========================================================
                              FOOTER
    ==========================================================-->
    <?= $this->render('/layouts/footer') ?>

</div>
<? $this->endBody() ?>
</body>
</html>
<? $this->endPage() ?>
