<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 28.08.17
 * Time: 22:10
 */
?>

<?
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\Json;
?>

<?
$cpath = Yii::$app->request->pathInfo;
$link = ltrim(Url::toRoute(Json::decode($model->url)), '/');
?>

<?
$content = $model->is_link
    ? Html::a(
        mb_convert_case($model->label, MB_CASE_TITLE, "UTF-8"),
        $model->url ? Url::toRoute(Json::decode($model->url)) : ''
    )
    : Html::tag('span', $model->label)
?>

<?=
Html::tag('li',
    $content,
    ['class' => [$cpath === $link ? 'active' : '']]
)
?>

