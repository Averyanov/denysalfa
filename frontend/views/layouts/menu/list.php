<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 28.08.17
 * Time: 22:07
 */
?>

<?
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?=
ListView::widget([
    'dataProvider'  => $dataProvider,
    'layout'        => '{items}',
    'options'       => ['tag' => 'ul', 'class' => 'rd-navbar-nav'],
    'itemOptions'   => ['tag' => null],
    'itemView'       => 'item'
])
?>
