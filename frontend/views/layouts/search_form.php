<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 17:32
 */
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="rd-navbar-search">
    <?
    ActiveForm::begin([
        'action' => Url::toRoute(['site/search']),
        'method' => 'get',
        'options' => ['class' => 'rd-navbar-search-form'],
    ])
    ?>

    <?=
    Html::label(Html::textInput('ss',
            null,
            ['placeholder' => "Поиск..", 'autocomplete' => 'off']),
        'ss',
        ['class' => 'rd-navbar-search-form-input'])
    ?>

    <?=
    Html::button('', ['class' => 'rd-navbar-search-form-submit', 'type' => 'submit'])
    ?>

    <?
    ActiveForm::end()
    ?>
    <span class="rd-navbar-live-search-results"></span>
    <button class="rd-navbar-search-toggle" data-rd-navbar-toggle=".rd-navbar-search">
        <span class="line-2 pull-left offset-1"></span>
        <span class="line-2 pull-right offset-2"></span>
    </button>
</div>
