<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 29.08.17
 * Time: 14:32
 */
?>

<?
use yii\widgets\ListView;
?>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{items}',
    'options' => [
        'class' => 'owl-carousel owl-carousel-1',
        'data-md-margin' => '30',
        'data-nav' => 'true',
        'data-items' => 1,
        'data-md-items' => 2
    ],
    'itemOptions' => ['tag' => null],
    'itemView' => 'item'
])
?>
