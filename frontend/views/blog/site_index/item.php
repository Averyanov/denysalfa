<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 29.08.17
 * Time: 14:38
 */
?>

<div class="owl-item">
    <img src="<?= $model->image ?>" width="570" height="488"
         alt=""/>
    <h3><a href="#"> <?= $model->caption ?></a></h3>
    <h6 class="text-primary">
        <time
            datetime="<?= Yii::$app->formatter->asDate($model->date,'yyyy-MM') ?>">
            <?= Yii::$app->formatter->asDate($model->date,'MM / dd / yyyy') ?>
        </time>
    </h6>
    <p><?= $model->short_description ?></p>
</div>
