<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 11:03
 */
use yii\widgets\ListView;
?>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => '{pager}{items}{pager}',
    'options' => [
        'tag' => 'ul',
        'class' => 'list-2'
    ],
    'itemOptions' => [
        'tag' => 'li'
    ],
    'itemView' => 'item',
    'showOnEmpty' => false,
    'emptyText' => 'Статей, включающих в себя такие теги, не обнаружено.'
])
?>
