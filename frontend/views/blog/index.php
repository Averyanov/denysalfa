<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 29.08.17
 * Time: 15:23
 */
?>

<?

/* @var $this yii\web\View */

$this->title = 'Блог';
?>

<?
use yii\widgets\Pjax;
?>

<!-- Blog -->
<section class="well-sm-2">
    <div class="container" data-lightbox="gallery">
        <h2 class="line-3">Блог</h2>
        <p class="text-default-2 inset-1 offset-3 letter-spacing-1">Fresh real estate news</p>

        <? Pjax::begin() ?>

        <?= $this->render('/tag/list', ['dataProvider' => $tagDataProvider]) ?>

        <?= $this->render('/blog/list', ['dataProvider' => $articleDataProvider]) ?>

        <? Pjax::end() ?>

    </div>
</section>
<!-- END Blog -->
