<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 29.08.17
 * Time: 15:27
 */
?>

<?
use yii\helpers\Url;
?>

<li>
    <img width="1170" height="447" alt="" src="<?= $model->image ?>">
    <h3>
        <a href="#">
            <?= $model->caption ?>
        </a>
    </h3>
    <div>
        <time datetime="<?= Yii::$app->formatter->asDate($model->date, 'yyyy-MM') ?>">
            <span class="fa fa-calendar"></span>
            <? Yii::$app->formatter->locale = 'en-US' ?>
            <a href="#">
                <?= Yii::$app->formatter->asDate($model->date) ?>
            </a>
        </time>
        <p><span class="fa fa-user"></span><a href="#">Admin</a></p>
        <p><span class="fa fa-comments"></span><a href="#">0 Comments</a></p>
    </div>
    <p>
        <?= $model->short_description ?>
    </p>
    <a class="btn btn-xs btn-primary" href="<?= Url::toRoute(['blog/view', 'id' => $model->id]) ?>">
        Читать далее...
        <span></span>
    </a>
</li>
