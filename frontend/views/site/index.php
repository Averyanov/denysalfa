<?

/* @var $this yii\web\View */

$this->title = 'Продажа недвижимости';
?>
<!-- Welcome -->
<section class="well-xl">
    <div class="container">
        <?//= $content ?>
        <div class="row">
            <div class="col-md-8">
                <h2 class="line-3">Продажа недвижимости</h2>
                <p class="text-default-2 inset-1 offset-3 letter-spacing-1">
                    Почему покупать, продавать, арендовать элитную недвижимость в Одессе выгодно и надежно вместе с Денисом Альфа
                </p>
                <div class="row offset-4">
                    <div class="col-md-6">
                        <p class="text-default-3">
                            Агентство недвижимости “Альфа Недвижимость” и инвестиционное агентство “Альфа Инвест” открыты для вас 24 часа в сутки и готовы оказать любые услуги.
                        </p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-default-3">
                            Полный спектр услуг с недвижимостью от Альфа Недвижимость позволяет вам совершать операции с недвижимостью с минимальным участием на всех этапах подготовки и заключения сделки.
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item">Мы работаем только в Одессе и ближайших окрестностях</li>
                                <li class="list-group-item">Осуществляем все виды операций с жилой недвижимостью</li>
                                <li class="list-group-item">Работаем с коммерческой недвижимостью</li>
                                <li class="list-group-item">Выполняем покупку и продажу земельных участков</li>
                                <li class="list-group-item">Производим девелопмент объектов различного назначения</li>
                                <li class="list-group-item">Принимаем в управление ваши инвестиции в недвижимость.</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item">Мы помогаем вам сформировать наиболее полно ваш запрос</li>
                                <li class="list-group-item">Подбираем варианты, соответствующие запросу</li>
                                <li class="list-group-item">Оформляем все юридические документы по выбранному варианту</li>
                                <li class="list-group-item">Вы получаете желаемый результат и удовольствие.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 offset-6">
                <h2 class="line-3">Gallery</h2>
                <p class="text-default-2 inset-1 offset-3 letter-spacing-1">Best apartments for you</p>
                <div class="owl-carousel owl-carousel-2 offset-5"  data-nav="true" data-items="1">
                    <div class="owl-item">
                        <img width="370" height="170" alt="" src="/images/page-1_img01.jpg">
                    </div>
                    <div class="owl-item">
                        <img width="370" height="170" alt="" src="/images/page-1_img01_2.jpg">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END Welcome -->
<!-- Swiper -->
<section class="bg-1">
    <!-- Swiper -->
    <div class="swiper-container swiper-slider swiper-slider-1" data-height="498px" data-min-height="400px" data-autoplay="false">
        <div class="swiper-wrapper">
            <div class="swiper-slide well-md" >
                <div class="swiper-slide">
                    <div class="swiper-slide-caption">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-6 col-md-preffix-3">
                                    <blockquote class="quote">
                                        <p><span class="text-white">“</span></p>
                                        <p><q class="text-white">
                                                Если есть дорогая покупка, свидетельсвующая о превратно понимаемом снобизме, то это автомобиль. Автомобили падают в цене. Стоимость домов растет.
                                            </q></p>
                                        <p><cite class="text-white text-bold"><a href="#">Харви Маккей</a></cite></p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide well-md">
                <div class="swiper-slide-caption">
                    <div class="container text-center">
                        <div class="row">
                            <div class="col-md-6 col-md-preffix-3">
                                <blockquote class="quote">
                                    <p><span class="text-white">“</span></p>
                                    <p><q class="text-white">
                                            Она ощутима, прочна, красива. С моей точки зрения, она даже артистична. Я просто обожаю недвижимость.
                                        </q></p>
                                    <p><cite class="text-white text-bold"><a href="#">Дональд Трамп</a></cite></p>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-slide well-md" >
                <div class="swiper-slide">
                    <div class="swiper-slide-caption">
                        <div class="container text-center">
                            <div class="row">
                                <div class="col-md-6 col-md-preffix-3">
                                    <blockquote class="quote">
                                        <p><span class="text-white">“</span></p>
                                        <p><q class="text-white">
                                                Ну, легкомыслены… ну, что ж… и милосердия иногда стучиться в их сердца… обыкновенные люди… в общем, напоминают прежних… квартирный вопрос только испортил их…
                                            </q></p>
                                        <p><cite class="text-white text-bold"><a href="#">Михаил Булгаков</a></cite></p>
                                    </blockquote>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Swiper Pagination -->
        <div class="swiper-pagination" ></div>
    </div>
</section>
<!-- END Swiper -->
<!-- Properties -->
<section class="well-sm">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-7">
                <h2 class="line-3">12 причин</h2>
                <p class="text-default-2 inset-1 offset-3 letter-spacing-1">почему более 100 000 клиентов получили с нами лучший результат</p>
            </div>
            <div class="col-lg-4 col-md-5 offset-7 text-left text-md-right">
                <?/*<ul class="list-1">
                            <li class="line-5">
                                <a href="#">All</a>
                            </li>
                            <li>
                                <a href="#">USA</a>
                            </li>
                            <li>
                                <a href="#">Canada</a>
                            </li>
                            <li class="line-4">
                                <a href="#">Mexico</a>
                            </li>
                        </ul>*/?>
            </div>
        </div>
    </div>
    <div class="container offset-8">
        <div data-lightbox="gallery">
            <div class="owl-carousel" data-xs-margin="30" data-nav="true" data-items="1" data-sm-items="2"
                 data-md-items="4">
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img01_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img01.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3><a href="#"> Оперативность</a></h3>
                    <h6 class="text-primary">New York</h6>
                    <p>
                        От 2-х часов на получение полноценного ответа по вашему запросу.
                        Нам очень важно ваше первое впечатление о нас.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb ">
                        <a href="images/gallery/page-1_img02_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img02.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3><a href="#"> Результативность</a></h3>
                    <h6 class="text-primary">Saratoga</h6>
                    <p>
                        Наши сотрудники могут совершить покупку/продажу всего за Один день.
                        Мы не можем позволить вам получить не самый лучший вариант. Мы работаем, чтобы вы получали максимальное удовлетворение от результата.
                        Мы ценим ваше Время!
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img03_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img03.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3><a href="#">Репутация</a></h3>
                    <h6 class="text-primary">Orlando</h6>
                    <p>
                        Я работаю в недвижимости с 1998 года и за это время я понял, что результаты работы полностью определяются репутацией моей и моих агентств.
                        Именно поэтому мы полностью удовлетворяем спрос и предложение наших клиентов.
                        В благодарность за нашу работу наши клиенты рекомендует нас своим знакомым.
                        Каждую вторую сделку закрываем на основании эксклюзивного договора.
                        Нам доверяют оригиналы правоустанавливающих документов и ключи от объектов недвижимости.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3><a href="#">Работа по эксклюзивному договору</a></h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Осуществляем продажу строго в установленный срок.
                        Информацию о продаже размещаем во всех агентствах недвижимости в Одессе и в Украине.
                        Мы реализуем полноценный рекламный бюджет.
                        Такие объекты недвижимости обрабатываем в первую очередь.
                        Приоритетное размещение на сайте компании.
                        Продаем по максимально возможной цене.

                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Оплата за результат
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Вы не оплачиваете наше время работы. Вы оплачиваете достигнутый результат.
                        Авансовый платеж будет включен в стоимость агентских услуг и он не обязателен.
                        Аванс лишь подтверждает серьезность ваших намерений.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Лучшие технологии в работе с недвижимостью
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        В нашей работе мы используем современные технологии обработки больших объемов информации и управления проектами, новейшие технологии коммуникаций. Это позволяет нам быстрее других находить желаемые варианты при решении любых задач наших клиентов.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Лучшая команда профессионалов
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Я всегда постоянно занимаюсь самообразованием во многих направлениях и требую этого от своих сотрудников. Поэтому я создаю крупнейшую в Одессе команду специалистов высшей категории, чтобы наши клиенты получали максимальные результаты при минимальных их усилиях.
                        Подготовкой каждого риелтора занимаются ведущие специалисты в своем направлении и это приносит потрясающий результат для наших клиентов.
                        А мы получаем самое дорогое — благодарные отзывы за нашу работу.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Сотрудничество с правоохранительными органами
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Нам крайне важен уровень безопасности наших клиентов, поэтому мы активно  сотрудничаем с органами УМВД, УСБУ, Генеральной и Областной Прокуратурой, Органами Юстиции, ФС.
                        Это взаимодействие помогает своевременно выявлять и предотвращать действия недобросовестных собственников или мошенников на рынке недвижимости Одессы.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Бонусная система
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Наша работа нацелена дать вам больше, чем вы можете ожидать.
                        Разработанная система бонусов позволяет вам экономить во многих вопросах.
                        Какие бонусы вы можете получить вы узнаете при заключении сделки по купле/продаже недвижимости.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Конфиденциальность
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Мы живем и работаем в Одессе, поэтому мы знаем все и обо всех, к тому же мы профессионалы.
                        Любая конфиденциальная информация, доверенная нам, хранится так же надежно как и вклады в Швейцарских банках. Работаем тихо, но надежно и эффективно для вас.
                        Мы не обсуждаем такую информацию с третьими лицами.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Лучший партнер по строительству и ремонтам в Украине
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        С нами никакой ремонт для вас не будет стихийным бедствием.
                        Мы сотрудничаем с международной строительной компанией ИмиджКом — крупнейшей в Украине. Ее работу хорошо знают в Одессе, в Киеве и в Москве.
                        Она обеспечивает наивысший уровень качества и соблюдение сроков.
                        Благодаря этому сотрудничеству вы можете быстро и качественно выполнить ремонт любого уровня, от эконом до элит, косметический или капитальный, в помещении какого угодно целевого назначения.
                    </p>
                </div>
                <div class="owl-item">
                    <div class="thumb">
                        <a href="images/gallery/page-1_img04_original.jpg" data-lightbox="image">
                            <img src="/images/gallery/page-1_img04.jpg" width="270" height="236"
                                 alt="Image 1"/>
                            <div class="thumb__overlay"></div>
                        </a>
                    </div>
                    <h3>
                        <a href="#">
                            Самая крупная сеть партнеров
                        </a>
                    </h3>
                    <h6 class="text-primary">Ottawa</h6>
                    <p>
                        Мы работаем с самой крупной сетью партнеров в Одессе и Украине в отрасли.
                        Мы решаем поставленную задачу клиентов с Украины, из России, ближнего и дальнего зарубежья.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- END Properties -->
<!-- Email -->
<section class="well-xl-1 bg-2">
    <div class="container">
        <div class="row row-xs-center">
            <div class="col-md-6">
                <!-- RD Mailform -->
                <form class='rd-mailform rd-mailform-1' method="post" action="bat/rd-mailform.php">
                    <!-- RD Mailform Type -->
                    <input type="hidden" name="form-type" value="contact"/>
                    <!-- END RD Mailform Type -->
                    <fieldset>
                        <label data-add-placeholder>
                            <input type="text"
                                   name="email"
                                   placeholder="Email"
                                   data-constraints="@NotEmpty @Email"/>
                        </label>
                        <div class="mfControls text-center">
                            <button class="btn btn-md" type="submit">Подпишись</button>
                        </div>

                        <div class="mfInfo"></div>
                    </fieldset>
                </form>
                <!-- END RD Mailform -->
            </div>
        </div>
    </div>
</section>
<!-- END Email -->
<!-- News -->
<section class="well-sm-1">
    <div class="container">
        <h2 class="line-3">Блог</h2>
        <p class="text-default-2 inset-1 offset-3 letter-spacing-1">Последние..</p>
    </div>
    <div class="container offset-8">
        <?= $this->render('/blog/site_index/list', ['dataProvider' => $articleDataProvider]) ?>
    </div>
</section>
<!-- END News -->
<section>
    <!-- RD Google Map -->
    <div class="rd-google-map">
        <div id="google-map" class="rd-google-map__model" data-zoom="14" data-x="-73.9874068" data-y="40.643180"></div>
        <ul class="rd-google-map__locations">
            <li data-x="-73.9874068" data-y="40.643180">
                <p>9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
            </li>
        </ul>
    </div>
    <!-- END RD Google Map -->
</section>