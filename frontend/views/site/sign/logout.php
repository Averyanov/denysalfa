<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 05.09.17
 * Time: 19:14
 */

use yii\helpers\Html;
//\yii\helpers\VarDumper::dump(Yii::$app->user->identity,10,1);die;
?>

<?= Html::beginForm(['/site/logout'], 'post') ?>
<?= Html::submitButton(
    'Logout (' . Yii::$app->user->identity->username . ')',
    ['class' => 'btn btn-link logout sign']
)?>
<?= Html::endForm()?>
