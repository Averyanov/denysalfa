<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 19:21
 */
use yii\widgets\ListView;
?>

<main class="page-content">
    <section class="well-sm-2">
        <div class="container">
            <h2 class="line-3">Результаты поиска</h2>
            <p class="text-default-2 inset-1 offset-3 letter-spacing-1">
                по Вашему запросу
                <span class="search-string">
                    "<?= $search_string ?>"
                </span>
            </p>
                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => '{items}',
                    'itemView' => function ($model) use ($search_string) {
                        return $this->render('/search/list', [
                            'model' => $model,
                            'search_string' => $search_string
                        ]);
                    },
                ])
                ?>
        </div>
    </section>
</main>