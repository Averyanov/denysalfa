<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use borales\extensions\phoneInput\PhoneInput;

$this->title = 'Регистрация';
?>

<section class="well-sm-2">
    <div class="container">
        <?= Html::tag('h1', $this->title) ?>



        <div class="row">
            <?php $form = ActiveForm::begin([
                'id' => 'signup-form',
                'options' => [
                    'class' => ['rd-mailform', 'offset-11', 'col-md-6']
                ]
            ]); ?>

            <fieldset>

                <?= $form->field($model, 'username', [
                    'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder">Имя</span>{endLabel}',
                ]) ?>
                <?= $form->field($model, 'email', [
                    'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder">Email</span>{endLabel}',
                ]) ?>

                <?= $form->field($model, 'password', [
                    'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder">Пароль</span>{endLabel}',
                ])->passwordInput([
                    'onfocus'   => 'makeBone.call(this,"state-1")',
                    'onblur'    => 'makeBone.call(this,"state-1")',
                ]) ?>
<!--                mfPlaceHolder-->

                <?= $form->field($model, 'phone',[
                    'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder"></span>{endLabel}',
                ])->widget(PhoneInput::className(), [
                    'jsOptions' => [
                        'preferredCountries' => ['no', 'pl', 'ua'],
                    ],
                ]);
                ?>

                <div class="mfControls text-center">
                    <?= Html::submitButton($this->title, ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

                <div class="mfInfo"></div>

            </fieldset>

            <?php ActiveForm::end(); ?>

            <div class="col-md-6">

            </div>
        </div>
    </div>

</section>