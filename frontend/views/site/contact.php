<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Контакты';
?>

<!-- Contacts -->
<section class="well-sm-2">
    <div class="container" data-lightbox="gallery">
        <h2 class="line-3">
            <?= $this->title ?>
        </h2>
        <p class="text-default-2 inset-1 offset-3 letter-spacing-1">Чтобы узнать больше о моих проектах, пишите мне на почту</p>
        <div class="row offset-19">
            <div class="col-lg-4 col-md-5 address text-center text-md-left">
                <h2 class="text-primary">ПРОДАЖА НЕДВИЖИМОСТИ</h2>
                <div class="row offset-20">
                    <div class="col-md-7 ">
                        <dl class="contact_list">
                            <dt>Адрес:</dt>
                            <dd>
                                <div class="box">
                                    <div class="aside">
                                        <span class="fa fa-home"></span>
                                    </div>
                                    <div class="cnt cnt1">
                                        <p><a href="#"> 9870 St Vincent Place,
                                                Glasgow, DC 45 Fr 45.</a></p>
                                    </div>
                                </div>
                            </dd>
                            <dt>Номер телефона:</dt>
                            <dd>
                                <div class="box">
                                    <div class="aside">
                                        <span class="fa  fa-phone"></span>
                                    </div>
                                    <div class="cnt">
                                        <a href="callto:#">+1 800 603 6035</a>
                                    </div>
                                </div>
                            </dd>
                            <dt>Факс)):</dt>
                            <dd>
                                <div class="box">
                                    <div class="aside">
                                        <span class="fa  fa-phone"></span>
                                    </div>
                                    <div class="cnt">
                                        <a href="callto:#">+1 800 603 6035</a>
                                    </div>
                                </div>
                            </dd>
                            <dt>E-mail:</dt>
                            <dd>
                                <div class="box">
                                    <div class="aside">
                                        <span class="fa fa-envelope"></span>
                                    </div>
                                    <div class="cnt">
                                        <a href="mailto:alfakadorr@gmail.com">alfakadorr@gmail.com</a>
                                    </div>
                                </div>
                            </dd>
                        </dl>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-lg-preffix-2 col-md-6 col-md-preffix-1">
                <!-- RD Mailform -->
                <? $form = ActiveForm::begin([
                        'id' => 'contact-form',
                        'options' => [
                            'class' => ['rd-mailform', 'offset-11']
                        ]
                ]); ?>
                <fieldset>

                    <?= $form->field($model, 'name', [
                        'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder">Имя</span>{endLabel}',
                    ]) ?>

                    <?= $form->field($model, 'email', [
                        'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder">Email</span>{endLabel}',
                    ]) ?>

                    <?= $form->field($model, 'body', [
                        'template' => '{beginLabel}{input} <span class="mfValidation"></span><span class="mfPlaceHolder">Сообщение</span>{endLabel}',
                    ])->textarea(['rows' => 6]) ?>

                    <div class="mfControls text-center">
                        <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                    <div class="mfInfo"></div>

                </fieldset>

                <? ActiveForm::end(); ?>

                <!-- END RD Mailform -->
            </div>
        </div>
    </div>
</section>
<!-- END Contact us -->
<section>
    <!-- RD Google Map -->
    <div class="rd-google-map">
        <div id="google-map" class="rd-google-map__model" data-zoom="14" data-x="-73.9874068" data-y="40.643180"></div>
        <ul class="rd-google-map__locations">
            <li data-x="-73.9874068" data-y="40.643180">
                <p>9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
            </li>
        </ul>
    </div>
    <!-- END RD Google Map -->
</section>