<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 29.08.17
 * Time: 13:38
 */
?>
<div class="swiper-container swiper-slider" data-height="39.365853658536585365853658536585%" data-min-height="400px" data-autoplay="false">
    <div class="swiper-wrapper">
        <div class="swiper-slide" data-slide-bg="/images/page-01_slide01.jpg">
            <div class="swiper-slide-caption">
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="/images/page-01_slide02.jpg">
            <div class="swiper-slide-caption">
            </div>
        </div>
        <div class="swiper-slide" data-slide-bg="/images/page-01_slide03.jpg">
            <div class="swiper-slide-caption">
            </div>
        </div>
    </div>
    <!-- Swiper Pagination -->
    <div class="swiper-pagination" ></div>
</div>
