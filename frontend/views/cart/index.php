<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 05.09.17
 * Time: 15:38
 */
?>

<?
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
?>

<?
$this->title = 'Корзина';
?>

<section class="well-sm-2">
    <div class="container">
        <?= Html::tag('h1', $this->title) ?>

        <h3>
            Cтраница корзины, здесь что-то должно быть....
        </h3>
        <h3>
            Нужна модель продукта...
        </h3>

        <div class="article">
            <h4>
                Детали заказа
            </h4>

            <?=
            $model->order
                ? DetailView::widget([
                    'model' => $model->order,
                    'attributes' => [
                        'status',
                    ]
                ])
                : 'Пусто'
            ?>

            <h5>
                Список элементов заказа
            </h5>

            <?=
            ListView::widget([
                'dataProvider' => $model->getOrderItemDataProvider(),
                'layout' => '{items}',
                'itemView' => '/order/item/index',
                'emptyText' => 'Пусто'
            ])
            ?>
        </div>

    </div>

</section>

