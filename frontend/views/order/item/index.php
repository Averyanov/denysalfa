<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 05.09.17
 * Time: 18:11
 */

use yii\widgets\DetailView;
?>

<h6>
    Детали элемента
</h6>

<?=
DetailView::widget([
    'model' => $model,
    'attributes' => [
        'product_id'
    ]
])
?>
