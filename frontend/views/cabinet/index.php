<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 05.09.17
 * Time: 15:36
 */
use yii\helpers\Html;
use yii\widgets\DetailView;

?>

<?
$this->title = 'Кабинет';
?>

<section class="well-sm-2">
    <div class="container">
        <?= Html::tag('h1', $this->title) ?>

        <h3>
            Cтраница кабинета, здесь что-то должно быть....
        </h3>

        <div class="article">
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'email',
                    'status',
                    'created_at:date',
                    'updated_at:date',
                ]
            ])
            ?>
        </div>

    </div>

</section>
