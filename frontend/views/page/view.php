<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 04.09.17
 * Time: 12:47
 */
use yii\helpers\Html;
?>

<?
$this->title = $model->meta_title;
?>

<section class="well-sm-2">
    <div class="container">
        <?= Html::tag('h1', $model->h1) ?>

        <div class="article">
            <?= $model->content ?>
        </div>
    </div>

</section>