<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 20:02
 */
use yii\widgets\ListView;
?>

<?=
ListView::widget([
    'dataProvider' => $model,
    'layout' => '{items}',
    'options' => ['tag' => 'ul', 'class' => 'row'],
    'itemOptions' => ['tag' => 'li', 'class' => 'col-md-4'],
    'emptyText' => '...ничего не найдено...',
    'itemView' => function ($model) use ($search_string){
        return $this->render('/search/item', [
            'model' => $model,
            'search_string' => $search_string
        ]);
    }
])
?>
