<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 20:05
 */
use yii\helpers\Html;
use common\components\TextHelper as TX;
use yii\helpers\Url;
?>

<?
$content =
    Html::tag('h3',
        TX::showMatch($search_string, $model->caption),
        ['class' => 'offset-17']).

    Html::tag('p',
        TX::showMatch($search_string, $model->short_description).'..',
        ['class' => 'offset-17']).

    Html::tag('time',
        Yii::$app->formatter->asDate($model->date,'MM / dd / yyyy'),
        [
            'datatime' => Yii::$app->formatter->asDate($model->date,'yyyy-MM'),
            'class' => 'text-primary'
        ]);
?>

<?= Html::a($content, Url::toRoute(['blog/view', 'id' => $model->id]), ['class' => '']) ?>
