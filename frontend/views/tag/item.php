<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 10:44
 */
?>

<?
use yii\helpers\Url;
use yii\helpers\Html;
?>
<div class="item <?= $model->getClass('selected') ?>">
    <?=
    Html::a(
        mb_strtolower($model->name),
        Url::toRoute($model->getHref(['blog', 'index'])),
        ['class' => ['link']]
    );
    ?>
</div>