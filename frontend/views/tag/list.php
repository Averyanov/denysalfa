<?
/**
 * Created by PhpStorm.
 * User: sebulbawork
 * Date: 30.08.17
 * Time: 10:59
 */
use yii\widgets\ListView;
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\CloudAsset;

CloudAsset::register($this);
?>

<?
$clear_link = Html::a(
    'Очистить фильтр',
    Url::toRoute(['blog/index']),
    [ 'class' => 'clear-all' ]
);
?>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}$clear_link",
    'options' => ['class' => 'tag-cloud'],
    'itemOptions' => ['tag' => null],
    'itemView' => '/tag/item',
])
?>
