<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Url;
?>
<div class="site-index">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="<?= Url::toRoute(['menu/index']) ?>">Menu</a>
        </li>
        <li class="list-group-item">
            <a href="<?= Url::toRoute(['article/index']) ?>">Blog</a>
        </li>
        <li class="list-group-item">
            <a href="<?= Url::toRoute(['page/index']) ?>">Статические страницы</a>
        </li>
        <li class="list-group-item">
            <a href="<?= Url::toRoute(['product/index']) ?>">Товары</a>
        </li>
    </ul>
</div>
